# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Person.create(name: "Fernanda", email: "fernandalagrange_13@hotmail.es", birthdate: Date.new(1996,3,13), gender: 1, dni: "24571456", customer: true)
Person.create(name: "Rocco", email: "roccodefina12@gmail.com", birthdate: Date.new(1993,11,4), gender: 0, dni: "24695925", customer: true)
