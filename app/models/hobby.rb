# == Schema Information
#
# Table name: hobbies
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  kind        :integer
#  person_id   :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_hobbies_on_person_id  (person_id)
#

class Hobby < ApplicationRecord
  belongs_to :person
  enum kind: {indoors: 0, outdoors: 1}
end
